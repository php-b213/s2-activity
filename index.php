<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S2 Activity</title>
</head>
<body>
	<h1>Divisibles of Five</h1>
	<?php divisibleByFive(); ?> 


	<h1>Array Manipulation</h1>
	<?php array_push($students, 'John Smith') ?>
	<pre><?php var_dump($students) ?></pre>
	<pre><?php echo count($students) ?></pre>


	<?php array_push($students, 'Jane Smith') ?>
	<pre><?php var_dump($students) ?></pre>
	<pre><?php echo count($students) ?></pre>


	<?php array_shift($students) ?>
	<pre><?php var_dump($students) ?></pre>
	<pre><?php echo count($students) ?></pre>



</body>
</html>